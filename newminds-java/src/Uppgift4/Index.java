package Uppgift4;

public class Index {
	
	public static String[] reverse(String[] words) {
		String[] temp = new String[words.length];
		for(int i=0; i<words.length; i++) {
			temp[i] = words[words.length - 1 - i];
		}
		return temp;
	}

	public static String join(String[] words) {
		//String[] strings = {"Java", "is", "cool"};
		StringBuilder builder = new StringBuilder();

		for (String string : words) {
		    if (builder.length() > 0) {
		        builder.append(" ");
		    }
		    builder.append(string);
		}

		String string = builder.toString();
		return string;
	}
	
	public static void main(String[] args) {
		String sentence = "Ta den här meningen och skriv en metod som skriver ut orden i omvänd ordning med sista ordet först";
		String[] words = sentence.split(" ");
		String[] reversedWords = reverse(words);
		
		String reversedSentence = join(reversedWords);
		System.out.println(reversedSentence);
		
	}
}
