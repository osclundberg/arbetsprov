package Uppgift5;

public class Index {
	public static void main(String[] args) {
		int[] numbers = {-23, 12, 21, -1, -5, 45, 3, -17, 16};
		
		int highestSum = 0;
		int startIndex;
		int endIndex;
		
		int i = 0, j = 0;
		for(; i<numbers.length; i++){
			for(; j<numbers.length; j++){
				int sum = 0;
				for(int k = i; k<=j; k++){
					sum += numbers[k];
				}
				
				if(sum > highestSum) {
					highestSum = sum;
				}
			}
		}
		
		System.out.println(highestSum);
	}
}
