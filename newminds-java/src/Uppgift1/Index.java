import java.util.HashMap;

public class Index {
	
	public static int stopBy(int numberOfStops, int[] peachPrices, int[] cherryPrices) {
		int totalPrices[] = new int[peachPrices.length];
		for(int i = 0; i < totalPrices.length; i++) {
			totalPrices[i] = peachPrices[i] + cherryPrices[i];
		}
		
		int cheapestStandPrice = 10000;
		int index=0;
		for(int i = 0; i < totalPrices.length; i++) {
			if( cheapestStandPrice > totalPrices[i]) {
				cheapestStandPrice = totalPrices[i];
				index = i;
			}
		}
		return index;
	}
	
	public static void main(String[] args) {
		int[] peaches = {10, 9, 9, 5, 5};
		int[] cherries = {10, 9, 9, 4, 5};
		
		int address = stopBy(cherries.length, peaches, cherries);
		System.out.println(address);
		//"Address" is the first stand with the cheapest cherries and peaches
	}
	
}
