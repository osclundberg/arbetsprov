package Uppgift2;

public class Index {
	public static void main(String[] args) {
		
		for(int i=1; i <= 100; i++) {
			if( (i % 3 == 0) && (i % 5 == 0) ) {
				System.out.println("New Minds");
			}
			else if(i % 3 == 0) {
				System.out.println("New");
			}
			else if(i % 5 == 0) {
				System.out.println("Minds");
			}
			else {
				System.out.println(i);
			}
		}
	}
}
