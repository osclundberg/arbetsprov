package Uppgift3;

public class Animal {
	public String name;
	public int age;
	public double weight;
	
	public String toString() {
		return "name: " +name + ", "
				+ "age: "+ age + ", " 
				+ "weight: " + weight;
	}
}
