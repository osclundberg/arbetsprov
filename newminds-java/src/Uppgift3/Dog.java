package Uppgift3;

public class Dog extends Animal{
	public String food;
	
	public Dog(String name, int age, double weight, String food){
		this.name = name;
		this.age = age;
		this.weight = weight;
		this.food = food;
	}
	
}
