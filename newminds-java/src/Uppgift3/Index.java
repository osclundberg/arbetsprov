package Uppgift3;

import java.util.ArrayList;
import java.util.List;

public class Index {
	
	public static Animal calculateOldestAnimal(ArrayList<Animal> animals) {
		Animal possibleOldestAnimal = animals.get(0);
		for(int i=0; i<animals.size(); i++) {
			if(animals.get(i).age > possibleOldestAnimal.age) {
				possibleOldestAnimal = animals.get(i);
			}
		}
		
		return possibleOldestAnimal;
	}
	
	public static void main(String[] args) {
		
		ArrayList<Animal> animals = new ArrayList<Animal>();
		
		Cow cow1 = new Cow("Mamma mu", 7, 243, 1000, 0.23);
		Cow cow2 = new Cow("Cow mu", 2, 143, 10200, 0.13);
		
		Dog dog1 = new Dog("fido", 1, 23, "Prima hundfoder");
		Dog dog2 = new Dog("lassie", 1, 23, "Prima hundfoder");
	
		Sheep sheep1 = new Sheep("frank", 12, 50, 21,"red");
		Sheep sheep2 = new Sheep("sheepy", 10, 50, 21,"brown");
		
		animals.add(cow1);
		animals.add(cow2);
		animals.add(dog1);
		animals.add(dog2);
		animals.add(sheep1);
		animals.add(sheep2);
		
		Animal oldestAnimal = calculateOldestAnimal(animals);
		System.out.println(oldestAnimal);
	}
}
