package Uppgift3;
//"baa", 12, 50, 21,"red"

public class Sheep extends Animal{
	public double wool;
	public String woolColor;
	
	public Sheep(String name, int age, double weight, double wool, String woolColor){
		this.name = name;
		this.age = age;
		this.weight = weight;
		this.wool = wool;
		this.woolColor = woolColor;
	}

}
