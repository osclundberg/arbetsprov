package Uppgift3;

public class Cow extends Animal{
	public double milkProduced;
	public double creamPercentage;
	
	public Cow(String name, int age, double weight, double milkProduced, double creamPercentage){
		this.name = name;
		this.age = age;
		this.weight = weight;
		this.milkProduced = milkProduced;
		this.creamPercentage = creamPercentage;
	}
	
	public String toString() {
		return "name: " +name + ", "
				+ "age: "+ age + ", " 
				+ "weight: " + weight + ", " 
				+ "milkProduced: " + milkProduced + ", "
				+ "creamPercentage: " + creamPercentage + ", "
				;	
	}
}
