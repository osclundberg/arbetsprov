using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using TaxCalculatorInterviewTests;

namespace TaxCalculatorInterviewTests {

    public interface ITaxCalculator {
        double GetStandardTaxRate(TaxCalculator.Commodity commodity);
        void SetCustomTaxRate(TaxCalculator.Commodity commodity, double rate);
        double GetTaxRateForDateTime(TaxCalculator.Commodity commodity, DateTime date);
        double GetCurrentTaxRate(TaxCalculator.Commodity commodity);
    }

    /// <summary>
    /// Implements a tax calculator for our client.
    /// The calculator has a set of standard tax rates that are hard-coded in the class.
    /// It also allows our client to remotely set new, custom tax rates.
    /// Finally, it allows the fetching of tax rate information for a specific commodity and point in time.
    /// Notes: We think there are a few bugs in the code below, since the calculations look messed up every now and then.
    ///       There are also a number of things that have to be implemented.
    /// </summary>
    public class TaxCalculator : ITaxCalculator {

        Dictionary<Commodity, List<Tuple<DateTime, double>>> _customRates = new Dictionary<Commodity, List<Tuple<DateTime, double>>>();

        public enum Commodity {
            //PLEASE NOTE: THESE ARE THE ACTUAL TAX RATES THAT SHOULD APPLY, WE JUST GOT THEM FROM THE CLIENT!
            Default,            //25%
            Alcohol,            //25%
            Food,               //12%
            FoodServices,       //12%
            Literature,         //6%
            Transport,          //6%
            CulturalServices    //6%
        }

        /// <summary>
        /// Get the standard tax rate for a specific commodity.
        /// </summary>
        public double GetStandardTaxRate(Commodity commodity) {
            switch (commodity) {
                case Commodity.Default: return 0.25;
                case Commodity.Alcohol: return 0.25;
                case Commodity.Food: return 0.12;
                case Commodity.FoodServices: return 0.12;
                case Commodity.Literature: return 0.6;
                case Commodity.Transport: return 0.6;
                case Commodity.CulturalServices: return 0.6;
                // XXX
                // Is this correct? It seems redundant when Default Commodity Tax Rate exists.
                // If not, should throw Exception "Cant get standard tax rate for unknown tax commodity" or similar.
                default: return 0.25;
            }
        }


        /// <summary>
        /// This method allows the client to remotely set new custom tax rates.
        /// When they do, we save the commodity/rate information as well as the UTC timestamp of when it was done.
        /// NOTE: Each instance of this object supports a different set of custom rates, since we run one thread per customer.
        ///       For now we just save it in memory, will use a database later.
        /// Supports saving multiple custom rates for different combinations of Commodity/DateTime
        /// Makes sure we never save duplicates, in case of e.g. clock resets, DST etc - overwrites old values if this happens
        /// </summary>
        public void SetCustomTaxRate(Commodity commodity, double rate) {
            if (!_customRates.ContainsKey(commodity)) {
                _customRates[commodity] = new List<Tuple<DateTime, double>>();
            }
            DateTime now = DateTime.UtcNow;

            _customRates[commodity].RemoveAll(item => item.Item1 == now); // Remove potential duplicates

            _customRates[commodity].Add(Tuple.Create(now, rate));

            _customRates[commodity].Sort(delegate(Tuple<DateTime, double> a, Tuple<DateTime, double> b) {
                return a.Item1.CompareTo(b.Item1);
            });
        }


        /// <summary>
        /// Gets the tax rate that is active for a specific point in time (in UTC).
        /// A custom tax rate is seen as the currently active rate for a period from its starting timestamp until a new custom rate is set.
        /// If there is no custom tax rate for the specified date, use the standard tax rate.
        /// </summary>
        public double GetTaxRateForDateTime(Commodity commodity, DateTime date) {
            if (!_customRates.ContainsKey(commodity)) return GetStandardTaxRate(commodity);
            DateTime utcDate = date.ToUniversalTime();

            List<Tuple<DateTime, double>> rates = _customRates[commodity];

            if (rates.Count == 0 || rates[0].Item1 > date) {
                return GetStandardTaxRate(commodity);
            }

            Tuple<DateTime, double> prev = Tuple.Create(DateTime.MinValue, -1.0); // Dummy for lower bound, will never hit
            foreach (Tuple<DateTime, double> current in rates) {
                if (utcDate >= prev.Item1 && utcDate < current.Item1) {
                    return prev.Item2;
                }

                prev = current;
            }

            return prev.Item2;
        }

        /// <summary>
        /// Gets the tax rate that is active for the current point in time.
        /// A custom tax rate is seen as the currently active rate for a period from its starting timestamp until a new custom rate is set.
        /// If there is no custom tax currently active, use the standard tax rate.
        /// </summary>
        public double GetCurrentTaxRate(Commodity commodity) {
            return GetTaxRateForDateTime(commodity, DateTime.UtcNow);
        }

    }
}

public class Program {
    public static void Main() {
        // TODO(oscjo) Put tests into a test suite

        TaxCalculator calc = new TaxCalculator();

        // Test Case: Should insert and retrieve custom rates correctly
        DateTime d0 = DateTime.Now;
        Thread.Sleep(1000);
        calc.SetCustomTaxRate(TaxCalculator.Commodity.Alcohol, 15);
        DateTime d1 = DateTime.Now;
        Thread.Sleep(1000);
        calc.SetCustomTaxRate(TaxCalculator.Commodity.Alcohol, 10);
        DateTime d2 = DateTime.Now;
        Thread.Sleep(1000);
        calc.SetCustomTaxRate(TaxCalculator.Commodity.Alcohol, 5);
        DateTime d3 = DateTime.Now;
        Thread.Sleep(1000);

        Debug.Assert(calc.GetTaxRateForDateTime(TaxCalculator.Commodity.Alcohol, d0) == 0.25); // No Available rate for date
        Debug.Assert(calc.GetTaxRateForDateTime(TaxCalculator.Commodity.Alcohol, d1) == 15); // Available rate between dates
        Debug.Assert(calc.GetTaxRateForDateTime(TaxCalculator.Commodity.Alcohol, d3) == 5); // Available rate with no "upper" date

        // Test Case: Should get correct standard tax rates
        Debug.Assert(calc.GetStandardTaxRate(TaxCalculator.Commodity.Default) == 0.25);
        Debug.Assert(calc.GetStandardTaxRate(TaxCalculator.Commodity.Alcohol) == 0.25);
        Debug.Assert(calc.GetStandardTaxRate(TaxCalculator.Commodity.Food) == 0.12);
        Debug.Assert(calc.GetStandardTaxRate(TaxCalculator.Commodity.FoodServices) == 0.12);
        Debug.Assert(calc.GetStandardTaxRate(TaxCalculator.Commodity.Literature) == 0.6);
        Debug.Assert(calc.GetStandardTaxRate(TaxCalculator.Commodity.Transport) == 0.6);
        Debug.Assert(calc.GetStandardTaxRate(TaxCalculator.Commodity.CulturalServices) == 0.6);

        Console.WriteLine("DONE");
    }
}